﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FSDP.UI.Models
{
    public class ContactViewModel
    {
        [Display(Name ="Full Name")]
        [Required()]
        public string Name { get; set; }

        [Display(Name = "Your Email")]
        [Required()]
        [EmailAddress()]
        public string Email { get; set; }

        [Display(Name = "Subject")]
        [Required()]
        public string Subject { get; set; }

        [Display(Name = "Message")]
        [Required()]
        public string Message { get; set; }

    }
}