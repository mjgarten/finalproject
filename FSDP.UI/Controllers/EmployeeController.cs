﻿using FSDP.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FSDP.UI.Controllers
{
    public class EmployeeController : Controller
    {
        private fsdpEntities db = new fsdpEntities();
        // GET: Employee
        public ActionResult LessonViewsByUser(int id)
        {
            var lessonViews = db.LessonViews.Where(x => x.UserID.Equals(id));
            return View(lessonViews.ToList());
        }
    }
}