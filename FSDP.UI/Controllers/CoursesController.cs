﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FSDP.Data;
using Microsoft.AspNet.Identity;

namespace FSDP.UI.Controllers
{
    [Authorize]
    public class CoursesController : Controller
    {
        private fsdpEntities db = new fsdpEntities();
        UnitOfWork uow = new UnitOfWork();

        // GET: Courses
        public ActionResult Index()
        {
            var courses1 = db.Courses1.Include(c => c.CourseCategory);
            return View(courses1.ToList());
        }

        //GET: MY Courses - show list of courses where a lesson has been viewed by current user


        // GET: Courses/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
                        
            Course course = db.Courses1.Find(id);
            string userId = User.Identity.GetUserId();
            
            ViewBag.completionPct = uow.CourseRepository.GetCompletionPercentage(course.CourseID, userId);

            var viewedLessons = from c in db.Courses1
                                join l in db.Lessons on c.CourseID equals l.CourseID
                                join lv in db.LessonViews on l.LessonID equals lv.LessonID
                                where c.IsActive == true
                                where l.IsActive == true
                                where l.CourseID == id
                                where lv.UserID == userId
                                where l.LessonViews.Count > 0
                                select c;


            ViewBag.CourseCount = viewedLessons.Count();
           
            return View(course);

        }

        // GET: Courses/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.CourseCategories, "CategoryID", "Name");
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseID,Name,Description,IsActive,CategoryID,CourseImage")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Courses1.Add(course);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.CourseCategories, "CategoryID", "Name", course.CategoryID);
            return View(course);
        }

        // GET: Courses/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses1.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.CourseCategories, "CategoryID", "Name", course.CategoryID);
            return View(course);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseID,Name,Description,IsActive,CategoryID,CourseImage")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.CourseCategories, "CategoryID", "Name", course.CategoryID);
            return View(course);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AjaxPublish(int id)
        {
            Course course = db.Courses1.Find(id);
            //update IsActive to true aka "publish"
            course.IsActive = true;
            db.Entry(course).State = EntityState.Modified;
            db.SaveChanges();

            //provide a means to confirm action complete back to user
            var message = string.Format("{0} is now published", course.Name);

            //return Json data for broswer to use to manipulate the page it's already on.
            //Here: creating JS properties with values
            //id of course and confirmation message
            return Json(new { id = id, message = message });
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AjaxUnpublish(int id)
        {
            Course course = db.Courses1.Find(id);
            //update IsActive to false aka "unpublish"
            course.IsActive = false;
            db.Entry(course).State = EntityState.Modified;
            db.SaveChanges();

            //provide a means to confirm action complete back to user
            var message = string.Format("{0} is now unpublished", course.Name);

            //return Json data for broswer to use to manipulate the page it's already on.
            //Here: creating JS properties with values
            //id of course and confirmation message
            return Json(new { id = id, message = message });
        }

        // GET: Courses/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses1.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course course = db.Courses1.Find(id);
            db.Courses1.Remove(course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
