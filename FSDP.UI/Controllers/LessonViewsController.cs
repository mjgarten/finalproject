﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FSDP.Data;
using Microsoft.AspNet.Identity;
using System.Web.Mail;
using System.Net.Mail;

namespace FSDP.UI.Controllers
{
    //[Authorize]
    public class LessonViewsController : Controller
    {
        private fsdpEntities db = new fsdpEntities();
        UnitOfWork uow = new UnitOfWork();

        // GET: LessonViews
        public ActionResult Index()
        {
            string user = User.Identity.GetUserId();
            var lessonViews = db.LessonViews.Where(x => x.UserID.Equals(user)).Include(l => l.Lesson);
            return View(lessonViews.ToList());
        }

        //GET: LessonViews only for current User
        [Authorize(Roles ="Admin, Manager")]
        public ActionResult ManagerView(string id)
        {
            var lessonViews = db.LessonViews.Where(u => u.AspNetUser.FirstName.Contains(id)).Include(l => l.Lesson).Include(u => u.AspNetUser);

            return View(lessonViews.ToList());
        }

        // GET: LessonViews/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LessonView lessonView = db.LessonViews.Find(id);
            if (lessonView == null)
            {
                return HttpNotFound();
            }
            return View(lessonView);
        }

        [AllowAnonymous]
        public ActionResult Add(int id)
        {
            LessonView item = new LessonView();
            item.LessonID = id;
            item.UserID = User.Identity.GetUserId();
            item.DateViewed = DateTime.Now;

            var existingView = db.LessonViews
                .Where(x => x.LessonID.Equals(item.LessonID))
                .Where(x => x.UserID.Equals(item.UserID))
                .FirstOrDefault();

           
            //db.Employees.Where(x => x.FirstName.Contains(e.FirstName)).Where(x => x.LastName.Contains(e.LastName)).FirstOrDefault();

            if (existingView == null)
            {
                db.LessonViews.Add(item);
                db.SaveChanges();
            }
                                          
            //TODO: redirect to lesson details view for current LessonID
            return RedirectToAction("Details", "Lessons", new { id = item.LessonID });
        }

        // GET: LessonViews/Create
        public ActionResult Create()
        {
            ViewBag.LessonID = new SelectList(db.Lessons, "LessonID", "Title");
            return View();
        }

        // POST: LessonViews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LessonViewID,UserID,LessonID,DateViewed")] LessonView lessonView)
        {
            if (ModelState.IsValid)
            {
                db.LessonViews.Add(lessonView);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LessonID = new SelectList(db.Lessons, "LessonID", "Title", lessonView.LessonID);
            return View(lessonView);
        }

        // GET: LessonViews/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LessonView lessonView = db.LessonViews.Find(id);
            if (lessonView == null)
            {
                return HttpNotFound();
            }
            ViewBag.LessonID = new SelectList(db.Lessons, "LessonID", "Title", lessonView.LessonID);
            return View(lessonView);
        }

        // POST: LessonViews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LessonViewID,UserID,LessonID,DateViewed")] LessonView lessonView)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lessonView).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LessonID = new SelectList(db.Lessons, "LessonID", "Title", lessonView.LessonID);
            return View(lessonView);
        }

        // GET: LessonViews/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LessonView lessonView = db.LessonViews.Find(id);
            if (lessonView == null)
            {
                return HttpNotFound();
            }
            return View(lessonView);
        }

        // POST: LessonViews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LessonView lessonView = db.LessonViews.Find(id);
            db.LessonViews.Remove(lessonView);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
