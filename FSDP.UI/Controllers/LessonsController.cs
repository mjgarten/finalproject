﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FSDP.Data;
using System.IO;//for file upload
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace FSDP.UI.Controllers
{
    [Authorize]
    public class LessonsController : Controller
    {
        private fsdpEntities db = new fsdpEntities();

        // GET: Lessons
        public ActionResult Index()
        {
            var lessons = db.Lessons.Include(l => l.Cours);
            return View(lessons.ToList());
        }


        //public ActionResult Details(int id)
        //{
        //    Lesson lesson = db.Lessons.Where(x => x.LessonID.Equals(id)).FirstOrDefault();

        //    LessonView item = new LessonView();
        //    item.LessonID = id;
        //    item.UserID = User.Identity.GetUserId();
        //    item.DateViewed = DateTime.Now;

        //    var existingView = db.LessonViews
        //        .Where(x => x.LessonID.Equals(item.LessonID))
        //        .Where(x => x.UserID.Equals(item.UserID))
        //        .FirstOrDefault();

        //    //db.Employees.Where(x => x.FirstName.Contains(e.FirstName)).Where(x => x.LastName.Contains(e.LastName)).FirstOrDefault();

        //    if (existingView == null)
        //    {
        //        db.LessonViews.Add(item);
        //        db.SaveChanges();


        //        Course course = db.Courses1.Where(x => x.CourseID.Equals(lesson.CourseID)).FirstOrDefault();
        //        string userId = User.Identity.GetUserId();

        //        var viewedLessons = from c in db.Courses1
        //                            join l in db.Lessons on c.CourseID equals l.CourseID
        //                            join lv in db.LessonViews on l.LessonID equals lv.LessonID
        //                            where c.IsActive == true
        //                            where l.CourseID == id
        //                            where lv.UserID == userId
        //                            where l.LessonViews.Count > 0
        //                            select c;


        //        //Course.Lessons.Count - ViewBag.CourseCount) == 0
        //        if ((course.Lessons.Count - viewedLessons.Count()) == 0)
        //        {
        //            CourseCompletion cc = new CourseCompletion();
        //            cc.CourseID = lesson.CourseID;
        //            cc.UserID = User.Identity.GetUserId();
        //            cc.DateCompleted = DateTime.Now;

        //            var existingCompletion = db.CourseCompletions
        //                                    .Where(x => x.CourseID.Equals(cc.CourseID))
        //                                    .Where(x => x.UserID.Equals(cc.UserID))
        //                                    .FirstOrDefault();


        //            if (existingCompletion == null)
        //            {
        //                //save completion to the database only ONE TIME
        //                db.CourseCompletions.Add(cc);
        //                db.SaveChanges();
        //            }
        //            }

        //    }

        //    return View(lesson);

        //}

        //GET: Lessons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            string userId = User.Identity.GetUserId();
            Lesson lesson = db.Lessons.Find(id);
            Course course = db.Courses1.Find(lesson.CourseID);
            CourseCompletion completion = new CourseCompletion();
            completion.CourseID = course.CourseID;
            completion.UserID = userId;
            completion.DateCompleted = DateTime.Now;

            var existingView = db.CourseCompletions.Where(x => x.CourseID.Equals(course.CourseID))
                                                   .Where(x => x.UserID.Equals(completion.UserID))
                                                   .FirstOrDefault();


            var viewedLessons = from c in db.Courses1
                                join l in db.Lessons on c.CourseID equals l.CourseID
                                join lv in db.LessonViews on l.LessonID equals lv.LessonID
                                where c.IsActive == true
                                where l.IsActive == true
                                where l.CourseID == course.CourseID
                                where lv.UserID == userId
                                where l.LessonViews.Count > 0
                                select c;

            var activeLessons = course.Lessons.Where(x => x.IsActive == true).Count();
            var courseCount = viewedLessons.Count();
            if (existingView == null)
            {
                if ((activeLessons - courseCount) == 0)
                {
                    db.CourseCompletions.Add(completion);
                    db.SaveChanges();

                    //create the body for the email
                    string subject = Session["FirstName"] + " " + Session["LastName"] + " completed " + course.Name;
                    string message = "All lessons have been viewed";

                    string body = string.Format(
                        "Name: {0}<br/>Email: {1}<br/>Subject: {2}<br/>Message:<br/>{3}",
                        /*User.Identity.Name*/
                        Session["FirstName"],
                        User.Identity.GetUserName(),
                        subject,
                        message
                        );
                    //create and configure the Mail Message
                    MailMessage msg = new MailMessage(
                        "no-reply@mjgarten.net",
                        "mjgartendev@gmail.com",
                        subject,
                        body
                        );

                    //configure the Mail Message object
                    msg.IsBodyHtml = true;

                    //create and configure the SMTP client
                    SmtpClient client = new SmtpClient(
                        "mail.mjgarten.net");
                    client.Credentials = new NetworkCredential("no-reply@mjgarten.net", "noreply32!");

                    using (client)
                    {
                        try
                        {
                            client.Send(msg);
                        }
                        catch
                        {
                            ViewBag.ErrorMessage = "There was an error sending your email. Please try again.";
                            return View();
                        };
                    }
                }
            }


            ViewBag.CourseName = course.Name;
            ViewBag.TotalLessons = activeLessons;
            ViewBag.CourseCount = viewedLessons.Count();

            //Youtube video player stuff
            if (lesson.VideoURL != null)
            {


                string CompleteYouTubeURL = lesson.VideoURL;

                var v = CompleteYouTubeURL.IndexOf("v=");
                var amp = CompleteYouTubeURL.IndexOf("&", v);
                string vid;
                // if the video id is the last value in the url
                if (amp == -1)
                {
                    vid = CompleteYouTubeURL.Substring(v + 2);
                    // if there are other parameters after the video id in the url
                }
                else
                {
                    vid = CompleteYouTubeURL.Substring(v + 2, amp - (v + 2));
                }
                ViewBag.VideoURL = vid;
            }



            if (lesson == null)
            {
                return HttpNotFound();
            }
            return View(lesson);
        }

        // GET: Lessons/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name");
            return View();
        }

        // POST: Lessons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LessonID,Title,CourseID,Introduction,VideoURL,PdfFilename,IsActive")] Lesson lesson, HttpPostedFileBase PdfFilename)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string fileName = "";
                    if (PdfFilename != null)
                    {
                        //string pdfExt = Path.GetExtension(PdfFilename.FileName).ToLower();
                        //string[] allowedExtensions = { ".pdf", ".docx" };
                        //if (allowedExtensions.Contains(pdfExt)
                        //{

                        //save file to the server
                        fileName = Path.GetFileName(PdfFilename.FileName);
                        string savePath = Server.MapPath("~/Content/Files/");
                        PdfFilename.SaveAs(savePath + fileName);
                        //}
                    }
                    //save filename to Database
                    lesson.PdfFilename = fileName;
                }
                catch (Exception)
                {

                    throw;
                }

                //Youtube URL upload thing
                //var v = lesson.VideoURL.IndexOf("v=");
                //var amp = lesson.VideoURL.IndexOf("&", v);
                //string vid;
                //// if the video id is the last value in the url
                //if (amp == -1)
                //{
                //    vid = lesson.VideoURL.Substring(v + 2);
                //    // if there are other parameters after the video id in the url
                //}
                //else
                //{
                //    vid = lesson.VideoURL.Substring(v + 2, amp - (v + 2));
                //}
                //lesson.VideoURL = vid;

                db.Lessons.Add(lesson);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name", lesson.CourseID);
            return View(lesson);
        }

        // GET: Lessons/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson lesson = db.Lessons.Find(id);
            if (lesson == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name", lesson.CourseID);
            return View(lesson);
        }

        // POST: Lessons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LessonID,Title,CourseID,Introduction,VideoURL,PdfFilename,IsActive")] Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lesson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name", lesson.CourseID);
            return View(lesson);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AjaxPublish(int id)
        {
            Lesson lesson = db.Lessons.Find(id);
            lesson.IsActive = true;
            db.Entry(lesson).State = EntityState.Modified;
            db.SaveChanges();

            //provide a means to confirm action complete back to user
            var message = string.Format("{0} is now published", lesson.Title);

            //return Json data for broswer to use to manipulate the page it's already on.
            //Here: creating JS properties with values
            //id of publishe rand our confirmation message
            return Json(new { id = id, message = message });
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AjaxUnpublish(int id)
        {
            Lesson lesson = db.Lessons.Find(id);
            lesson.IsActive = false;
            db.Entry(lesson).State = EntityState.Modified;
            db.SaveChanges();

            //Confirmation message to display to user
            var message = string.Format("{0} is now unpublished", lesson.Title);

            //Create JS properties with values (id of lesson & confirmation message)
            return Json(new { id = id, message = message });
        }

        // GET: Lessons/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson lesson = db.Lessons.Find(id);
            if (lesson == null)
            {
                return HttpNotFound();
            }
            return View(lesson);
        }

        // POST: Lessons/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Lesson lesson = db.Lessons.Find(id);
            db.Lessons.Remove(lesson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
