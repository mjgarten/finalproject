﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FSDP.Data;
using System.Net.Mail;
using Microsoft.AspNet.Identity;
using FSDP.UI.Models;

namespace FSDP.UI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private fsdpEntities db = new fsdpEntities();

        public ActionResult Index()
        {
            string id = User.Identity.GetUserId();
            ViewBag.CompletedCourses = db.CourseCompletions.Where(x => x.UserID.Equals(id)).Where(x => x.Cours.IsActive == true).Count();

            var courses1 = db.Courses1.Include(c => c.CourseCategory);
            return View(courses1.ToList());
        }

        
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //create the body for the email 
            string body = string.Format("Name: {0}</br>Email: {1}</br></hr>{2}",contact.Name, contact.Email, contact.Message);
                            
            //create and configure the Mail Message
            MailMessage msg = new MailMessage(
                "no-reply@mjgarten.net",
                "mjgartendev@gmail.com",
                contact.Subject,
                body
                );

            //configure the Mail Message object
            msg.IsBodyHtml = true;

            //create and configure the SMTP client
            SmtpClient client = new SmtpClient(
                "mail.mjgarten.net");
            client.Credentials = new NetworkCredential("no-reply@mjgarten.net", "noreply32!");

            using (client)
            {
                try
                {
                    client.Send(msg);
                }
                catch
                {
                    ViewBag.ErrorMessage = "There was an error sending your email. Please try again.";
                    return View();
                };
            }


            //send them to a confirmation view and send the Mail Message object with it

            return ModelState.IsValid ? View("ContactConfirmation", contact) : View();

        }//end contact overload
    }
}
