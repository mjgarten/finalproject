﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FSDP.Data;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace FSDP.UI.Controllers
{
    //[Authorize(Roles = "Admin, Manager")]
    public class CourseCompletionsController : Controller
    {

        private fsdpEntities db = new fsdpEntities();

        // GET: CourseCompletions
        public ActionResult Index()
        {
            string user = User.Identity.GetUserId();
            var courseCompletions = db.CourseCompletions.Include(c => c.Cours).Where(x => x.UserID.Equals(user));

            return View(courseCompletions.ToList());
        }

        [Authorize(Roles = "Admin, Manager")]
        public ActionResult ManagerView()
        {
            var courseCompletions = db.CourseCompletions.Include(c => c.Cours).Include(u => u.AspNetUser);

            return View(courseCompletions.ToList());
        }


        // GET: CourseCompletions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseCompletion courseCompletion = db.CourseCompletions.Find(id);
            if (courseCompletion == null)
            {
                return HttpNotFound();
            }
            return View(courseCompletion);
        }

        // GET: CourseCompletions/Create
        public ActionResult Create()
        {
            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name");
            return View();
        }

        // POST: CourseCompletions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseCompletionID,UserID,CourseID,DateCompleted")] CourseCompletion courseCompletion)
        {
            if (ModelState.IsValid)
            {
                db.CourseCompletions.Add(courseCompletion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name", courseCompletion.CourseID);
            return View(courseCompletion);
        }

        [AllowAnonymous]
        public ActionResult Add(int id)
        {
            CourseCompletion item = new CourseCompletion();
            item.CourseID = id;
            item.UserID = User.Identity.GetUserId();
            item.DateCompleted = DateTime.Now;

            var existingCompletion = db.CourseCompletions
                                    .Where(x => x.CourseID.Equals(item.CourseID))
                                    .Where(x => x.UserID.Equals(item.UserID))
                                    .FirstOrDefault();

            //db.Employees.Where(x => x.FirstName.Contains(e.FirstName)).Where(x => x.LastName.Contains(e.LastName)).FirstOrDefault();

            if (existingCompletion == null)
            {
                //save completion to the database only ONE TIME
                db.CourseCompletions.Add(item);
                db.SaveChanges();

                //send email to employee's manager & CC employee
                Course completedCourse = db.Courses1.Where(x => x.CourseID.Equals(item.CourseID)).FirstOrDefault();
                 
                //create the body for the email
                string subject = Session["FirstName"] + " " + Session["LastName"] + " completed " + completedCourse.Name;
                string message = "All lessons have been viewed";

                string body = string.Format(
                    "Name: {0}<br/>Email: {1}<br/>Subject: {2}<br/>Message:<br/>{3}",
                    /*User.Identity.Name*/
                    Session["FirstName"],
                    User.Identity.GetUserName(),
                    subject,
                    message
                    );
                //create and configure the Mail Message
                MailMessage msg = new MailMessage(
                    "no-reply@mjgarten.net",
                    "mjgartendev@gmail.com",
                    subject,
                    body
                    );

                //configure the Mail Message object
                msg.IsBodyHtml = true;

                //create and configure the SMTP client
                SmtpClient client = new SmtpClient(
                    "mail.mjgarten.net");
                client.Credentials = new NetworkCredential("no-reply@mjgarten.net", "noreply32!");

                using (client)
                {
                    try
                    {
                        client.Send(msg);
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was an error sending your email. Please try again.";
                        return View();
                    };
                }
            }


            //TODO: redirect to lesson details view for current LessonID
            return RedirectToAction("Index", "CourseCompletions");
        }

        // GET: CourseCompletions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseCompletion courseCompletion = db.CourseCompletions.Find(id);
            if (courseCompletion == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name", courseCompletion.CourseID);
            return View(courseCompletion);
        }

        // POST: CourseCompletions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseCompletionID,UserID,CourseID,DateCompleted")] CourseCompletion courseCompletion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(courseCompletion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseID = new SelectList(db.Courses1, "CourseID", "Name", courseCompletion.CourseID);
            return View(courseCompletion);
        }

        // GET: CourseCompletions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseCompletion courseCompletion = db.CourseCompletions.Find(id);
            if (courseCompletion == null)
            {
                return HttpNotFound();
            }
            return View(courseCompletion);
        }

        // POST: CourseCompletions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CourseCompletion courseCompletion = db.CourseCompletions.Find(id);
            db.CourseCompletions.Remove(courseCompletion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
