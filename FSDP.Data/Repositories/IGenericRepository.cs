﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        //interfaces provide method stubs for implementing classes to define

        List<TEntity> Get(string includeProperties = "");
        TEntity Find(object id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(object id);
        void Remove(TEntity entity);

        int CountRecords();

    }
}

