﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data.Repositories
{
    public class LessonRepository : GenericRepository<Lesson>
    {
        public LessonRepository(DbContext db) : base(db) { }
        //this specific repository iplements the generic version but for author. No extra code is needed unless you want extra goodies specific to Author-only
    }
}
