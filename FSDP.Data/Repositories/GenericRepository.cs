﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        //private cStoreEntities db = new cStoreEntities();
        internal DbContext db;
        //make a new ctor that accepts the db object on creation
        public GenericRepository(DbContext context)
        {
            this.db = context;
        }

        //v1 - too simple, doesn't account for complex types like Titles
        //public List<TEntity> Get()
        //{
        //    return db.Set<TEntity>().ToList();
        //}

        //v2 - accommodates more complex types, like Titles
        public virtual List<TEntity> Get(string includeProperties = "")
        {
            IQueryable<TEntity> query = db.Set<TEntity>();

            foreach (var prop in includeProperties.Split(
                new char[] { ',' },
                    StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(prop);
            }
            return query.ToList();
        }

        public TEntity Find(object id)
        {
            return db.Set<TEntity>().Find(id);
        }

        public void Add(TEntity entity)
        {
            db.Set<TEntity>().Add(entity);
            //db.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            db.Set<TEntity>().Remove(entity);
            //db.SaveChanges();
        }

        public void Remove(object id)
        {
            TEntity entity = Find(id);
            Remove(entity);
        }

        public int CountRecords()
        {
            return Get().Count();
        }
    }
}
