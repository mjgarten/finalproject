﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data.Repositories
{
    public class LessonViewRepository : GenericRepository<LessonView>
    {
        public LessonViewRepository(DbContext db) : base(db) { }
    }
}
