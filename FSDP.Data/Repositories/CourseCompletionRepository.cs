﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data.Repositories
{
    public class CourseCompletionRepository : GenericRepository<CourseCompletion>
    {
        public CourseCompletionRepository(DbContext db) : base(db) { }
    }
}
