﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data.Repositories
{
    public class CourseRepository : GenericRepository<Course>
    {
        public CourseRepository(DbContext db) : base(db) { }

        public int GetCompletionPercentage(int id, string userId)
        {
            Course course = Find(id);
            var activeLessons = course.Lessons.Where(l => l.IsActive == true);
            var viewedLessons = activeLessons.Where(lv => lv.LessonViews.Where(u => u.UserID.Equals(userId)).Count() > 0);
           
            return viewedLessons.Count() * 100 / activeLessons.Count();
        }
    }
}
