﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data
{
    [MetadataType(typeof(CourseCompletionMetadata))]
    public partial class CourseCompletion { }

    public class CourseCompletionMetadata
    {
        [Display(Name = "Employee")]
        [Required]
        public string UserID { get; set; }

        [Required]
        [Display(Name = "Course")]
        public int CourseID { get; set; }

        [Display(Name = "Completion Date")]
        [DisplayFormat(DataFormatString ="{0:d}")]
        public System.DateTime DateCompleted { get; set; }

    }
}
