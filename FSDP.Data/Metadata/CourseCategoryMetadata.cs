﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data
{
    [MetadataType(typeof(CourseCategoryMetadata))]
    public partial class CourseCategory { }

    public class CourseCategoryMetadata
    {
        [Required]
        [StringLength(100)]
        [Display(Name ="Category")]
        public string Name { get; set; }

        [StringLength(250)]
        public string Description { get; set; }


    }
}
