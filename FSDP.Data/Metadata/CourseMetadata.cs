﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data
{
    [MetadataType(typeof(CourseMetadata))]
    public partial class Course { }

    public class CourseMetadata
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [Required]
        [Display(Name="Is Course Active?")]
        public bool IsActive { get; set; }
        public Nullable<int> CategoryID { get; set; }

        [Display(Name="Course Image")]
        [StringLength(100)]
        public string CourseImage { get; set; }
    }
}
