﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data
{
    [MetadataType(typeof(LessonMetadata))]
    public partial class Lesson { }

    public class LessonMetadata
    {
        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Display(Name = "Course")]
        [Required]
        public int CourseID { get; set; }

        [StringLength(300)]
        public string Introduction { get; set; }

        [StringLength(250)]
        [Display(Name ="Video URL")]
        public string VideoURL { get; set; }

        [StringLength(100)]
        [Display(Name ="Lesson PDF")]
        public string PdfFilename { get; set; }

        [Display(Name ="Is Active?")]
        public bool IsActive { get; set; }
    }
}
