﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data
{
    [MetadataType(typeof(LessonViewMetadata))]
    public partial class LessonView { }

    public class LessonViewMetadata
    {
        [Required]
        [StringLength(128)]
        [Display(Name ="Employee")]
        public string UserID { get; set; }

        [Required]
        [Display(Name ="Lesson")]
        public int LessonID { get; set; }

        [Required]
        [Display(Name ="View Date")]
        public System.DateTime DateViewed { get; set; }

    }
}
