﻿using FSDP.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSDP.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
    }

    public class UnitOfWork : IUnitOfWork
    {
        internal fsdpEntities context = new fsdpEntities();

        //Courses
        private CourseRepository courseRepository;//field
        public CourseRepository CourseRepository
        {
            get
            {
                if (this.courseRepository == null)
                {
                    this.courseRepository = new CourseRepository(context);
                }
                return courseRepository;
            }
        }
        //CourseCompletions
        private CourseCompletionRepository courseCompletionRepository;//field
        public CourseCompletionRepository CourseCompletionRepository
        {
            get
            {
                if (this.courseCompletionRepository == null)
                {
                    this.courseCompletionRepository = new CourseCompletionRepository(context);
                }
                return courseCompletionRepository;
            }
        }
        //Lessons
        private LessonRepository lessonRepository;//field
        public LessonRepository LessonRepository
        {
            get
            {
                if (this.lessonRepository == null)
                {
                    this.lessonRepository = new LessonRepository(context);
                }
                return lessonRepository;
            }
        }
        //LessonViews
        private LessonViewRepository lessonViewRepository;//field
        public LessonViewRepository LessonViewRepository
        {
            get
            {
                if (this.lessonViewRepository == null)
                {
                    this.lessonViewRepository = new LessonViewRepository(context);
                }
                return lessonViewRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        #region Dispose()
        //Dispose() often required. It removes object from memory

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();//calls parameterless
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);//line that disposes object 
        }

        #endregion

    }
}
